global main
extern printf
extern atoi
extern sqrt

section .data
	result_msg: db 'sqrt(%d) = %d', 10, 0
	param_number_error_msg: db 'Expected parameter count to be 1 but %d found.', 10, 0
	param_no_number_error_msg: db 'Expected parameter to be a positive number.', 10, 0
	nomatch_msg: db 'sqrt(%d) not found.', 10, 0

section .text
main:
	mov eax, [esp+4]		;Get argc
	cmp eax, 2			;Compare to 2
	jne param_number_error		;If not 2 go to number_error
	mov eax, [esp+8]		;Get argv
	mov eax, [eax+4]		;Get second param
	push eax			;Push param
	call atoi			;Call atoi
	add esp, 4			;Fix stack
	cmp eax, 0			;Compare atoi result to 0
	je param_no_number_error	;If 0 go to param_no_number_error
	mov ebx, eax			;Move eax to ebx to make sure it persists
	push ebx			;Push ebx
	call sqrt			;Call sqrt function
	pop ebx				;Pop ebx
	cmp eax, 0			;Compare eax (result) to 0
	je nomatch			;If result is 0 go to nomatch
finish:
	push eax			;Push eax
	push ebx			;Push ebx
	push result_msg			;Push result_msg
	call printf			;Print
	add esp, 12			;Fix stack
	ret				;Return
param_no_number_error:
	push param_no_number_error_msg	;Push param_no_number_error_msg
	call printf			;Print
	add esp, 4			;Fix stack
	ret				;Return
param_number_error:
	dec eax				;Decrement param count
	push eax			;Push param count
	push param_number_error_msg	;Push param_number_error_msg
	call printf			;Print
	add esp, 8			;Fix stack
	ret				;Return
nomatch:
	push ebx			;Push ebx
	push nomatch_msg		;Push nomatch_msg
	call printf			;Print
	add esp, 8			;Fix stack
	ret				;Return
