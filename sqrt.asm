global sqrt
extern printf

section .data
	dbg: db '%d', 10, 0

section .text
sqrt:
	push ebx
	mov ebx, [esp+8]
	mov ecx, ebx
sqrt_check:
	mov eax, ecx		;Put b into eax
	imul eax, eax		;b²
	mov edx, ecx		;Put b into edx
	inc edx			;b+1
	imul edx, edx		;(b+1)²
	cmp eax, ebx		;Compare b² to a
	jl sqrt_error		;If less return error
	je sqrt_finish		;If equal -> result found
	jmp sqrt_loop		;If not less or equal jump to sqrt_loop
sqrt_loop:
	sub eax, eax		;Set eax to 0
	sub edx, edx		;Set edx to 0
	mov eax, ebx		;c into eax
	div ecx			;eax = (c/b)
	add eax, ecx		;eax = (b+(c/b))
	sar eax, 1		;eax = (b+(c/b)) / 2
	mov ecx, eax		;new b to ecx
	jmp sqrt_check		;Check
sqrt_finish:
	mov eax, ecx		;Move to be return value
	pop ebx			;Pop ebx
	ret			;Return
sqrt_error:
	mov eax, 0		;Move 0 to eax
	pop ebx			;Pop ebx
	ret			;Return
